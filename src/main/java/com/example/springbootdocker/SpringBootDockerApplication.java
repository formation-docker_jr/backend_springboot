package com.example.springbootdocker;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author jrobert
 */
@SpringBootApplication
@Repository
@Controller
public class SpringBootDockerApplication {

    /**
     *
     * @param args
     */
    public static void main(String[] args) {
        SpringApplication.run(SpringBootDockerApplication.class, args);
    }

    @Autowired
    private JdbcTemplate jdbcTemplate;

    /**
     *
     * @return la liste des nom des utilisateurs
     */
    @RequestMapping("/api/utilisateur")
    @ResponseBody
    public List<String> home() {
            return this.jdbcTemplate.queryForList(
                    "SELECT nom FROM utilisateur", String.class);
    }

}
